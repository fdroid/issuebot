#!/usr/bin/env python3
#
# issuebot_apt_install = python3-bs4

import gitlab
import inspect
import json
import logging
import os
import re
import requests
import sys
import unicodedata
from androguard.core.bytecodes.apk import get_apkid
from bs4 import BeautifulSoup
from urllib.parse import urlunsplit

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
import issuebot
from issuebot import IssuebotModule
from fdroidserver import deploy


class LinksToServices(IssuebotModule):
    def search_androidobservatory(self):
        url = 'https://androidobservatory.org/?searchby=pkg&q=' + self.application_id
        print('Searching %s...' % url)
        r = issuebot.requests_get(url)
        if r.status_code == 200:
            soup = BeautifulSoup(r.text, 'lxml')
            for a in soup.find_all('a'):
                if self.application_id == a.text.strip():
                    self.appid_links.append(
                        {'name': 'Android Observatory APKScan', 'url': url}
                    )
                    break
        baseurl = 'https://androidobservatory.org/?searchby=hash&q='
        for filename, checksums in self.apk_checksums.items():
            url = baseurl + checksums['sha256']
            r = issuebot.requests_get(url)
            if r.status_code == 200:
                soup = BeautifulSoup(r.text, 'lxml')
                for a in soup.find_all('a'):
                    if self.application_id == a.text.strip():
                        self.add_label('in-androidobservatory')
                        self.apk_links.append(
                            {
                                'name': 'Android Observatory APKScan',
                                'apk': filename,
                                'url': 'https://androidobservatory.org'
                                + a.parent.parent.a['href'],
                            }
                        )
                        break

    def search_apkmirror(self):
        url = (
            'https://www.apkmirror.com/?post_type=app_release&searchtype=apk&s='
            + self.application_id
        )
        print('Searching %s...' % url)
        r = issuebot.requests_get(url)
        if r.status_code == 200:
            soup = BeautifulSoup(r.text, 'lxml')
            for a in soup.find_all('a'):
                children = a.parent.parent.findChildren()
                if a.parent.name == 'h5' and len(children) > 2:
                    path = a.attrs.get('href')
                    if path:
                        self.add_label('in-apkmirror')
                        self.appid_links.append(
                            {
                                'name': 'APKMirror: %s' % a.text.strip(),
                                'url': urlunsplit(
                                    ('https', 'www.apkmirror.com', path, None, None)
                                ),
                            }
                        )
                        break

    def search_exodus_privacy(self):
        headers = dict(issuebot.HEADERS)
        token = os.getenv('EXODUS_PRIVACY_TOKEN', None)
        if not token:
            print('ERROR: cannot get exodus privacy token:')
            return
        headers['Authorization'] = 'Token ' + token
        url = 'https://reports.exodus-privacy.eu.org/api/search/' + self.application_id
        print('Searching %s...' % url)
        r = requests.get(url, headers=headers, timeout=issuebot.REQUESTS_TIMEOUT)
        if r.status_code == 200:
            data = r.json()
            if self.application_id in data:
                reports = data[self.application_id].get('reports', [])
                if len(reports) > 0:
                    self.add_label('in-exodus-privacy')
                    self.appid_links.append(
                        {
                            'name': 'Exodus Privacy Reports',
                            'url': (
                                'https://reports.exodus-privacy.eu.org/en/reports/search/%s/'
                                % self.application_id
                            ),
                        }
                    )
                    self.reply['reportData']['exodus-privacy'] = reports
                    for report in reports:
                        if len(report.get('trackers', [])) > 0:
                            self.add_label('trackers')
                            break

    def search_gitlab(self):
        """Search for issues and merge requests, with 20 retries"""
        print('Searching https://gitlab.com/fdroid/...')
        found_issues = None
        found_mergerequests = None
        i = 0
        while i < 20:
            i += 1
            try:
                gl = self.get_gitlab_api()
                group = gl.groups.get('fdroid')
            except gitlab.exceptions.GitlabSearchError as e:
                print('ERROR: cannot get gitlab group:', e)
                break
            except AttributeError as e:
                print('ERROR: cannot get gitlab API key:', e)
                break
            try:
                issues = group.search('issues', self.application_id)
                if len(issues) > 0:
                    found_issues = {
                        'name': 'F-Droid GitLab Issues',
                        'url': (
                            'https://gitlab.com/groups/fdroid/-/issues?scope=all&utf8=%E2%9C%93&state=opened&search='
                            + self.application_id
                        ),
                    }
            except gitlab.exceptions.GitlabSearchError as e:
                print('ERROR: search issues:', e)
            try:
                merge_requests = group.search('merge_requests', self.application_id)
                if len(merge_requests) > 0:
                    found_mergerequests = {
                        'name': 'F-Droid GitLab Merge Requests',
                        'url': (
                            'https://gitlab.com/groups/fdroid/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&search='
                            + self.application_id
                        ),
                    }
            except gitlab.exceptions.GitlabSearchError as e:
                print('ERROR: search merge requests:', e)
            if found_issues and found_mergerequests:
                break
        if found_issues:
            self.appid_links.append(found_issues)
        if found_mergerequests:
            self.appid_links.append(found_mergerequests)

    def search_google_play(self):
        url = 'https://play.google.com/store/apps/details?id=' + self.application_id
        print('Searching %s...' % url)
        r = issuebot.requests_head(url)
        if r.status_code == 200:
            self.add_label('in-google-play')
            self.appid_links.append({'name': 'Google Play', 'url': url})

    def search_koodous(self):
        url = 'https://api.koodous.com/apks/'
        print('Searching %s...' % url)
        for filename, checksums in self.apk_checksums.items():
            r = issuebot.requests_get(url + checksums['sha256'])
            if r.status_code == 200:
                self.add_label('in-koodous')
                data = r.json()
                self.apk_links.append(
                    {
                        'name': 'Koodous',
                        'apk': filename,
                        'url': 'https://koodous.com/apks/' + checksums['sha256'],
                    }
                )
                if 'koodous' not in self.reply['reportData']:
                    self.reply['reportData']['koodous'] = dict()
                self.reply['reportData']['koodous'][filename] = data

    def search_izzysoft(self):
        url = 'https://apt.izzysoft.de/fdroid/index/apk/' + self.application_id
        print('Searching %s...' % url)
        r = issuebot.requests_head(url)
        if r.status_code == 200:
            self.add_label('in-izzysoft')
            self.appid_links.append({'name': 'IzzySoft', 'url': url})

    def search_pithus(self):
        url = 'https://beta.pithus.org/report/'
        print('Searching %s...' % url)
        for filename, checksums in self.apk_checksums.items():
            r = issuebot.requests_head(url + checksums['sha256'])
            if r.status_code == 200:
                self.add_label('in-pithus')
                self.apk_links.append(
                    {
                        'name': 'Pithus',
                        'apk': filename,
                        'url': url + checksums['sha256'],
                    }
                )

    def _get_plexus_light(self, s):
        green = 'No reported issues'
        grey = 'X'
        if s == green:
            return '🟢', ''
        if s == grey:
            return '⚪', ''
        return '🔴', s

    def search_plexus(self):
        """Search the dataset in techlore/plexus

        The structure of the data looks very brittle, and they make
        big changes without keeping the old API, so just print out any
        errors that might be encountered, instead of breaking the
        whole module run.

        """
        url = 'https://raw.githubusercontent.com/techlore/Plexus-app/main/Plexus.json'
        print('Searching https://plexus.techlore.tech/...')
        r = self.requests_cache_get(url)
        try:
            r.raise_for_status()
            data = r.json()
            for app in data:
                if self.application_id == app['Package']:
                    self.add_label('in-plexus')

                    asc = ''.join(
                        c
                        for c in unicodedata.normalize('NFD', app['Application'])
                        if unicodedata.category(c) != 'Mn'
                    )
                    slug = re.sub(r'[^a-z0-9]', '-', asc.lower())

                    url = 'https://plexus.techlore.tech/applications/%s' % slug
                    self.appid_links.append(
                        {
                            'url': url,
                            'name': 'Plexus: %s de-Googled: %s (%s) | %s microG: %s (%s)'
                            % (
                                *self._get_plexus_light(app['DG_Notes']),
                                app['DG_Rating'],
                                *self._get_plexus_light(app['MG_Notes']),
                                app['MG_Rating'],
                            ),
                        }
                    )
                    self.reply['reportData']['plexus'] = app
        except Exception as e:
            print('ERROR:', e)

    def search_verification_fdroid_org(self):
        print('Searching https://verification.f-droid.org/...')
        url = 'https://verification.f-droid.org/%s.json' % self.application_id
        r = self.requests_cache_get(url)
        try:
            r.raise_for_status()
            data = r.json()
            apkReport = data['apkReports'][-1]
            name = 'verification.fdroid.org: %s ' % apkReport[:-5]
            link_url = 'https://verification.f-droid.org/%s' % apkReport
            if data['lastRunVerified']:
                name += 'was reproduced.'
                self.add_label('reproducible-builds')
            else:
                name += 'failed to build reproducibly!'
                link_url = link_url.replace('.json', '.diffoscope.html')
            self.appid_links.append({'url': link_url, 'name': name})
        except Exception as e:
            print('ERROR:', e)

    def search_virustotal(self):
        if not hasattr(self, 'virustotal_api_key'):
            print('ERROR: virustotal_api_key not found')
            return
        print('Searching https://www.virustotal.com...')
        logging.getLogger().setLevel(logging.WARNING)
        os.makedirs(os.path.join(self.base_dir, 'virustotal'), exist_ok=True)
        for filename, checksums in self.apk_checksums.items():
            _ignored, versionCode, versionName = get_apkid('repo/' + filename)
            df = None
            try:
                df = deploy.upload_apk_to_virustotal(
                    self.virustotal_api_key,
                    self.application_id,
                    filename,
                    checksums['sha256'],
                    versionCode,
                    versionName=versionName,
                )
                self.add_label('in-virustotal')
                self.apk_links.append(
                    {
                        'name': 'VirusTotal',
                        'apk': filename,
                        'url': (
                            'https://www.virustotal.com/gui/file/%s/detection'
                            % checksums['sha256']
                        ),
                    }
                )
                with open(df) as fp:
                    data = json.load(fp)
                self.reply['reportData']['virustotal'] = data
            except Exception as e:
                print('ERROR: cannot get report for "%s":' % filename, e)

    def main(self):
        self.appid_links = []
        self.apk_links = []
        self.calc_apk_checksums()

        # self.search_androidobservatory()  # service is just returning 500 errors
        self.search_apkmirror()
        self.search_exodus_privacy()
        self.search_gitlab()
        self.search_google_play()
        self.search_izzysoft()
        self.search_koodous()
        self.search_pithus()
        self.search_plexus()
        self.search_verification_fdroid_org()
        self.search_virustotal()

        report = '<h3>Links to Services</h3>'
        if self.appid_links:
            report += '<details open><summary>by Application ID</summary><ul>'
            for link in self.appid_links:
                report += '<li><a href="{url}">{name}</a></li>'.format(**link)
            report += '</ul></details>'
        if self.apk_links:
            report += '<details><summary>by APK</summary><ul>'
            for link in self.apk_links:
                report += '<li><a href="{url}">{name}: <tt>{apk}</tt></a></li>'.format(
                    **link
                )
            report += '</ul></details>'
        self.reply['report'] = report
        self.reply['reportData']['applicationId'] = self.appid_links
        self.write_json()


if __name__ == "__main__":
    LinksToServices().main()

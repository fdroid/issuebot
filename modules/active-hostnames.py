#!/usr/bin/env python3
#
#
# issuebot_apt_install = python3-cffi python3-jinja2 python3-pycares python3-location libloc-database

import inspect
import jinja2
import os
import sys
import zipfile

import location

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule

# load vendored ipgrep
from importlib.util import spec_from_file_location, module_from_spec

spec = spec_from_file_location(
    'module.name', os.path.join(localmodule, '.vendor', 'ipgrep.py')
)
ipgrep = module_from_spec(spec)
spec.loader.exec_module(ipgrep)
ASN = ipgrep.ASN
Extractor = ipgrep.Extractor
Host = ipgrep.Host
Resolver = ipgrep.Resolver

J2_TEMPLATE = """
<h3>Active Hostnames and IP Addresses</h3>
{% for name, data in sections %}
<details {% if found_anti_features %}open{% endif %}><summary>{{ name }}</summary><table>
<tr><th>name</th><th>IP</th><th>ASN Description</th><th>Anti-Features</th></tr>
{% for name, ip, desc, anti_features in data %}
{% if anti_features %}
<tr><td><b>{{ name }}</b></td><td><tt>{{ ip }}</tt></td><td>{{ desc }}</td><td>🚩&nbsp;{{ anti_features }}</td></tr>
{% else %}
<tr><td><b>{{ name }}</b></td><td><tt>{{ ip }}</tt></td><td>{{ desc }}</td><td></td></tr>
{% endif %}
{% endfor %}
</table></details>
{% endfor %}
"""


class IPGrep(IssuebotModule):
    def main(self):
        # TODO support scanning the source code
        if not self.apkfiles_required():
            return
        self.apk_path = self.apkfiles[0]  # TODO actually go through all APKs
        if not os.access(self.apk_path, os.R_OK):
            return
        resolver = Resolver(timeout=120)
        ip_lookup = location.open()
        names, ips = set(), set()

        for path in self.apkfiles:
            for url in self.get_urls_from_apk(path):
                names.add(url.split('/')[2])

        try:
            with zipfile.ZipFile(self.apk_path) as z:
                for f in z.namelist():
                    for line in z.open(f).readlines():
                        extractor = Extractor(line)
                        names = names | set(extractor.extract_names())
                        ips = ips | set(extractor.extract_ips())
        except zipfile.BadZipFile as e:
            print(e, file=sys.stderr)
            sys.exit(1)

        resolved = resolver.resolve(names)
        hosts_fromnames = set(
            [Host(ip=i, name=n, query_type=q) for i, n, q in resolved]
        )
        hosts_fromips = set([Host(ip=ip) for ip in ips])
        hosts = hosts_fromnames | hosts_fromips

        for host in hosts:
            subnet = ip_lookup.lookup(host.ip)
            asn = ASN(0, "-", "-")
            if subnet:
                autonomous_system = ip_lookup.get_as(subnet.asn).name
                country = ip_lookup.get_country(subnet.country_code)
                country = country.name if country else subnet.country_code
                asn = ASN(
                    subnet.asn,
                    subnet.country_code,
                    "AS{}: {} ({})".format(
                        subnet.asn,
                        autonomous_system,
                        country,
                    ),
                )
            if not host.name:
                host.name = "-"
            host.asn = asn

        hostnames = []
        ipaddresses = []
        found_anti_features = False
        for host in sorted(hosts, key=lambda x: (x.name, x.ip, x.asn.description)):
            anti_features = set()
            if self.etip_network_search(host.name) or self.etip_network_search(host.ip):
                self.add_label('trackers')
                anti_features.add('Tracking')
                found_anti_features = True
            af = ','.join(sorted(anti_features))
            if host.name == host.ip:
                ipaddresses.append([host.name, host.ip, host.asn.description, af])
            else:
                hostnames.append([host.name, host.ip, host.asn.description, af])

        if hostnames or ipaddresses:
            env = jinja2.Environment(autoescape=True)
            template = env.from_string(J2_TEMPLATE)
            sections = [
                ('Hostnames', hostnames),
                ('IP addresses', ipaddresses),
            ]
            self.reply['report'] = template.render(
                sections=sections, found_anti_features=found_anti_features
            )
            self.reply['reportData'] = {
                'hostnames': hostnames,
                'ipAddresses': ipaddresses,
            }
        self.write_json()


if __name__ == '__main__':
    IPGrep().main()

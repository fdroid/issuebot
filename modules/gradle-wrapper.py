#!/usr/bin/env python3

import hashlib
import html
import inspect
import json
import os
import re
import sys
import textwrap

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), "..")
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
import issuebot
from issuebot import IssuebotModule


class GradleWrapper(IssuebotModule):
    def __init__(self):
        super().__init__()
        f = 'checksums.json'
        download = True
        if os.path.exists(f):
            with open(f) as fp:
                try:
                    self.checksums = json.load(fp)
                    download = False
                except Exception:
                    os.remove(f)
        while download:
            url = 'https://gitlab.com/fdroid/gradle-transparency-log/-/raw/master/checksums.json'
            print('Fetching', url)
            r = issuebot.requests_get(url)
            if r.status_code != 200:
                print(r.status_code, '- retrying')
            else:
                self.checksums = r.json()
                with open(f, 'w') as fp:
                    json.dump(self.checksums, fp)
                break
        self.sha256s = dict()
        self.urls = dict()
        for url, sha256s in self.checksums.items():
            for sha256_dict in sha256s:
                sha256 = sha256_dict['sha256']
                if sha256 not in self.urls:
                    self.urls[sha256] = []
                self.urls[sha256].append(url)
                if url not in self.sha256s:
                    self.sha256s[url] = []
                self.sha256s[url].append(sha256)

    def check_properties(self, root, f):
        path = os.path.join(root, f)
        display_path = os.path.relpath(path, self.source_dir)

        keys = issuebot.read_properties_keys(path)
        duplicates = [i for i in set(keys) if keys.count(i) > 1]
        if duplicates:
            self.report = (
                '<details open><summary><b>ERROR:</b></summary>'
                + '<em>gradle/wrapper/gradle-wrapper.properties</em>'
                + 'contains duplicate keys: %s!' % html.escape(','.join(duplicates))
                + '</details>'
                + self.report
            )

        propdata, properties = issuebot.read_properties(path)
        distributionUrl = properties['distributionUrl'].replace('\\:', ':')
        properties_url = os.path.join(
            self.get_current_commit_id_url(),
            'gradle/wrapper/gradle-wrapper.properties',
        )
        if not distributionUrl.startswith('https:'):
            self.add_label('insecure-gradlew')
            self.report = (
                textwrap.dedent(
                    f"""
                    <details open><summary><b>ERROR:</b></summary>
                    <a href="{properties_url}">
                    <em>gradle/wrapper/gradle-wrapper.properties</em></a>
                    must use HTTPS in <tt>distributionUrl</tt>!
                    </details>
                    """
                )
                + self.report
            )
            distributionUrl = distributionUrl.replace('http:', 'https:')
        if not distributionUrl.startswith(
            'https://services.gradle.org/distributions/gradle-'
        ):
            self.add_label('insecure-gradlew')
            self.report = (
                textwrap.dedent(
                    f"""
                    <details><summary>non-standard source</summary>
                    <a href="{properties_url}">
                    <em>gradle/wrapper/gradle-wrapper.properties</em></a>
                    uses non-standard source for downloading gradle:
                    {distributionUrl}</details>\n
                    """
                )
                + self.report
            )

        sha256s = self.sha256s.get(distributionUrl, [])
        distributionSha256Sum = properties.get('distributionSha256Sum')
        if distributionSha256Sum and sha256s and distributionSha256Sum not in sha256s:
            self.add_label('insecure-gradlew')
            self.report += (
                '* `distributionSha256Sum=%s` should be `distributionSha256Sum=%s`!\n'
                % (html.escape(distributionSha256Sum), html.escape(sha256s[0]))
            )
        if not distributionSha256Sum and sha256s:
            self.add_label('insecure-gradlew')
            self.report += textwrap.dedent(
                f"""
                <details><summary>missing <tt>distributionSha256Sum</tt></summary><p>
                <a href="{properties_url}">
                <em>gradle/wrapper/gradle-wrapper.properties</em></a> is missing
                <a href="https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:verification">
                <tt>distributionSha256Sum</tt></a>.

                This means that the gradle download is not verified. We recommend
                explicitly setting the expected SHA-256 to protect you and your apps
                if a bad actor gets access to the Gradle servers or manages to
                [MitM](https://max.computer/blog/how-to-take-over-the-computer-of-any-java-or-clojure-or-scala-developer/)
                your internet connection. Please note that Android Studio supports this
                poorly, with only a weird text prompt inline in a log window, but it
                does work there.

                Here is an example of how to fix this:

                ```properties
                """
            )
            self.report += """{propdata}\ndistributionSha256Sum={sha256}\n```\n\n</p></details>\n""".format(
                propdata=html.escape(propdata.strip()), sha256=html.escape(sha256s[0])
            )
        self.reportData[display_path] = {
            'distributionUrl': distributionUrl,
            'sha256SumOfDistributionUrl': sha256s,
            'distributionSha256Sum': distributionSha256Sum,
        }

    def check_jar(self, root, f):
        gradle_jar = os.path.join(root, f)
        hasher = hashlib.sha256()
        with open(gradle_jar, 'rb') as fp:
            hasher.update(fp.read())
        sha256 = hasher.hexdigest()
        properties_path = gradle_jar[:-4] + '.properties'
        propdata, properties = issuebot.read_properties(properties_path)
        distributionUrl = properties.get('distributionUrl', '').replace('\\://', '://')
        declared_url = re.sub(r'(bin|all).zip', r'wrapper.jar', distributionUrl)
        urls = self.urls.get(sha256)
        if not urls:
            local_url = None
        elif declared_url in urls:
            local_url = declared_url
        else:
            local_url = urls[-1]
        display_properties = os.path.relpath(properties_path, self.source_dir)
        display_jar = os.path.relpath(gradle_jar, self.source_dir)
        properties_link = self.get_source_url(display_properties)
        jar_link = self.get_source_url(display_jar)
        self.reportData[display_jar] = {
            'sha256': sha256,
            'localUrl': local_url,
            'properties': display_properties,
            'declaredUrl': declared_url,
            'distributionUrl': distributionUrl,
        }

        sha256s = self.sha256s.get(distributionUrl, [])
        if len(sha256s) == 0:
            correct_sha256 = (
                '1f3067073041bc44554d0efe5d402a33bc3d3c93cc39ab684f308586d732a80d'
            )
        else:
            correct_sha256 = sha256s[0]

        upgrading_howto = (
            '<p>There is a gradle command for [upgrading](%s) the wrapper:\n\n'
            '```bash\n./gradlew wrapper --gradle-version %s \\\n'
            '  --gradle-distribution-sha256-sum %s'
            '\n```\n\n</p>'
        ) % (
            'https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:upgrading_wrapper',
            os.path.basename(declared_url.replace('-wrapper.jar', '')).split('-')[-1]
            or '5.6.4',
            correct_sha256,
        )
        if not local_url:
            self.add_label('insecure-gradlew')
            self.add_label('non-free')
            self.report = (
                '<details open><summary><b>ERROR:</b></summary>\n'
                + '[_%s_](%s) cannot be verified as free software because it'
                + ' does not match any listed on the [official website]'
                + '(https://gradle.org/release-checksums/): <tt>%s</tt>'
                + upgrading_howto
                + '</details>'
            ) % (display_jar, jar_link, sha256) + self.report
        elif local_url != declared_url:
            self.add_label('insecure-gradlew')
            self.report += (
                '<details><summary>gradle vs wrapper version mismatch</summary>'
                + '<em>%s</em> is [%s](%s.sha256), but [_%s_](%s) declares <tt>%s</tt> as the gradle version.'
                + upgrading_howto
                + '</details>'
            ) % (
                display_jar,
                os.path.basename(local_url),
                local_url,
                display_properties,
                properties_link,
                distributionUrl,
            )

    def main(self):
        if not os.path.isdir(self.source_dir):
            print(self.source_dir, 'does not exist, skipping')
            return
        issuebot.run_cli_tool(
            ['git', '-C', os.path.relpath(self.source_dir), 'reset', '--hard']
        )
        self.report = ''
        self.reportData = dict()
        for root, dirs, files in os.walk(self.source_dir):
            for f in files:
                if f == 'gradle-wrapper.properties':
                    self.check_properties(root, f)
                elif f == 'gradle-wrapper.jar':
                    self.check_jar(root, f)

        if self.report:
            if 'insecure-gradlew' in self.reply['labels']:
                self.report += textwrap.dedent(
                    """
                    These security measures can prevent real world attacks that are
                    happening in the wild! See these blog posts for more info:\n
                    * https://blog.gradle.org/project-integrity
                    * https://commonsware.com/blog/2023/01/25/gradle-wrapper-supply-chain-attack.html
                    * https://commonsware.com/blog/2021/01/27/checking-poisoned-projects.html
                    """
                )
            self.reply['report'] = (
                self.get_source_scanning_header('Gradle Wrapper') + self.report
            )

        self.reply['reportData'][self.application_id] = self.reportData
        self.write_json()


if __name__ == '__main__':
    GradleWrapper().main()

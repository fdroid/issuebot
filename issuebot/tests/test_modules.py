#!/usr/bin/env python3

import issuebot
import os
import re
import textwrap
import unittest
from androguard.misc import AnalyzeAPK
from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import mock


class TestModules(unittest.TestCase):
    """Test the setup of the harness for running the modules"""

    def setUp(self):
        self.cwd = os.getcwd()
        self.environ = os.environ
        os.environ['CI'] = '1'

    def tearDown(self):
        for k in self.environ:
            os.environ[k] = self.environ[k]
        os.chdir(self.cwd)

    def test_get_modules(self):
        """get_modules should return the local modules when run like ./issuebot.py"""
        os.chdir(os.path.dirname(os.path.dirname(__file__)))
        self.assertTrue(issuebot.get_modules())

    def test_print_exc_flush(self):
        """print_exc_flush should print in the same timeline as print

        stdout is buffered but stderr is not, this method should sync things up.
        """
        print('BEFORE')
        try:
            int('this should throw an exception between "BEFORE" and "AFTER"')
        except ValueError:
            issuebot.print_exc_flush()
        print('AFTER', flush=True)

    def test_setup_for_modules_with_py(self):
        def run_cli_tool(cmd):
            if cmd[0] == 'apt-get':
                self.count += 1
                self.assertEqual('androguard', cmd[2])
                self.assertEqual('s3cmd', cmd[3])

        self.count = 0
        testfile = 'modules/foo.py'
        with TemporaryDirectory() as tmpdir, mock.patch(
            'issuebot.run_cli_tool', run_cli_tool
        ), mock.patch('issuebot.get_modules', lambda: [testfile]), mock.patch(
            'os.getuid', lambda: 0
        ):
            os.chdir(tmpdir)
            os.mkdir('modules')
            with open(testfile, 'w') as fp:
                fp.write('# issuebot_apt_install = androguard s3cmd\n')
            issuebot.setup_for_modules()
            with open(testfile, 'w') as fp:
                fp.write('#issuebot_apt_install = androguard s3cmd\n')
            issuebot.setup_for_modules()
            with open(testfile, 'w') as fp:
                fp.write('  #   issuebot_apt_install = androguard   s3cmd\n')
            issuebot.setup_for_modules()
            with open(testfile, 'w') as fp:
                fp.write('#\tissuebot_apt_install = androguard\ts3cmd\n')
            issuebot.setup_for_modules()
        self.assertEqual(4, self.count)

    def test_setup_for_modules_with_php(self):
        def run_cli_tool(cmd):
            if cmd[0] == 'apt-get':
                self.count += 1
                self.assertEqual('php-cli', cmd[2])
                self.assertEqual('apktool', cmd[3])

        self.count = 0
        testfile = 'modules/foo.php'
        with TemporaryDirectory() as tmpdir, mock.patch(
            'issuebot.run_cli_tool', run_cli_tool
        ), mock.patch('issuebot.get_modules', lambda: [testfile]), mock.patch(
            'os.getuid', lambda: 0
        ):
            os.chdir(tmpdir)
            os.mkdir('modules')
            with open(testfile, 'w') as fp:
                fp.write('// issuebot_apt_install = php-cli apktool\n')
            issuebot.setup_for_modules()
            with open(testfile, 'w') as fp:
                fp.write('/*\nissuebot_apt_install = php-cli apktool\n*/\n')
            issuebot.setup_for_modules()
            with open(testfile, 'w') as fp:
                fp.write('/*issuebot_apt_install = php-cli apktool*/\n')
            issuebot.setup_for_modules()
        self.assertEqual(3, self.count)

    def test_get_current_commit_id_url(self):
        test_tags = [
            (
                'https://github.com/ukanth/afwall',
                'v3.5.2',
                'https://github.com/ukanth/afwall/tree/v3.5.2',
            ),
            (
                'https://gitlab.com/fdroid/fdroidclient',
                '1.12.1',
                'https://gitlab.com/fdroid/fdroidclient/tree/1.12.1',
            ),
            (
                'https://codeberg.org/Arne/monocles_chat',
                'v1.1',
                'https://codeberg.org/Arne/monocles_chat/src/v1.1',
            ),
            (
                'https://notabug.org/Tobiwan/ffupdater',
                '74.1.0',
                'https://notabug.org/Tobiwan/ffupdater/src/74.1.0',
            ),
            (
                'https://framagit.org/tom79/fedilab-tube',
                '1.13.1',
                'https://framagit.org/tom79/fedilab-tube/tree/1.13.1',
            ),
            (
                'https://git.sr.ht/~anjan/lift',
                'v0.2',
                'https://git.sr.ht/~anjan/lift/tree/v0.2',
            ),
            (
                'https://git.jami.net/savoirfairelinux/ring-project',
                'android/release_296',
                'https://git.jami.net/savoirfairelinux/ring-project/tree/android/release_296',
            ),
            (
                'https://git.selfprivacy.org/kherel/selfprivacy.org.app',
                'v.0.1.2',
                'https://git.selfprivacy.org/kherel/selfprivacy.org.app/src/v.0.1.2',
            ),
            (
                'https://git.selfprivacy.org/kherel/selfprivacy.org.app',
                'd3f494adebc897ba003e059baf028a33c1e46336',
                'https://git.selfprivacy.org/kherel/selfprivacy.org.app/src/d3f494adebc897ba003e059baf028a33c1e46336',
            ),
        ]
        issuebot_module = issuebot.IssuebotModule()
        with TemporaryDirectory() as tmpdir:
            os.chdir(tmpdir)
            issuebot_module.source_dir = os.path.join(os.getcwd(), 'test')
            os.mkdir(issuebot_module.source_dir)
            os.mkdir(os.path.join(issuebot_module.source_dir, '.git'))
            for source_url, commit_id, tag_url in test_tags:
                issuebot_module.source_url = source_url
                self.assertEqual(
                    issuebot_module.get_current_commit_id_url(commit_id), tag_url
                )

    def test_get_job_issue_ids(self):
        if 'CI_JOB_ID' in os.environ:
            self.assertEqual(os.getenv('CI_JOB_ID'), issuebot.get_job_issue_ids()[0])
            del os.environ['CI_JOB_ID']
        if 'ISSUEBOT_CURRENT_ISSUE_ID' in os.environ:
            self.assertEqual(
                os.getenv('ISSUEBOT_CURRENT_ISSUE_ID'), issuebot.get_job_issue_ids()[1]
            )
            del os.environ['ISSUEBOT_CURRENT_ISSUE_ID']
        self.assertTrue('0', issuebot.get_job_issue_ids()[1])

    def test_run_cli_tool_smokecheck(self):
        """Test that cli tools work at the most basic level"""
        p = issuebot.run_cli_tool(['date'])
        print(p.stdout.decode())

    def test_run_cli_tool_timeout(self):
        """Test that things exit cleanly when the timeout is hit"""
        issuebot.run_cli_tool(['sleep', '2'], timeout=1)

    def test_run_cli_tool_error(self):
        """Test that things exit cleanly when the tool returns an error"""
        issuebot.run_cli_tool(['false'], timeout=1)

    @mock.patch.dict(os.environ)
    @mock.patch('sys.argv', ['modules/just-a-test.py'])
    def test_get_artifacts_url(self):
        os.environ = {
            'CI_JOB_ID': '123456',
            'CI_PAGES_DOMAIN': 'gitlab.io',
            'CI_PROJECT_NAME': 'rfp',
            'CI_PROJECT_ROOT_NAMESPACE': 'fdroid',
            'ISSUEBOT_CURRENT_ISSUE_ID': '321',
        }
        self.assertEqual(
            'https://fdroid.gitlab.io/-/rfp/-/jobs/123456/artifacts/public/issuebot/123456/321/just-a-test.py.html',
            issuebot.get_artifacts_url(),
        )

    @mock.patch.dict(os.environ)
    def test_get_artifacts_browse_url(self):
        os.environ = {
            'CI_JOB_URL': 'https://gitlab.com/fdroid/rfp/-/jobs/12345',
            'CI_JOB_ID': '12345',
            'ISSUEBOT_CURRENT_ISSUE_ID': '321',
        }
        self.assertEqual(
            'https://gitlab.com/fdroid/rfp/-/jobs/12345/artifacts/browse/public/issuebot/12345/321/',
            issuebot.get_artifacts_browse_url(),
        )

    def test_get_urls_from_apk_instance(self):
        i = issuebot.IssuebotModule()
        for path in (
            'issuebot/tests/info.zwanenburg.caffeinetile_4.apk',
            'issuebot/tests/minimal_targetsdk_30_unsigned.apk',
            'issuebot/tests/org.bitbucket.tickytacky.mirrormirror_4.apk',
        ):
            apk = AnalyzeAPK(path)[0]
            self.assertEqual([], i.get_urls_from_apk(apk))

    def test_get_urls_from_apk_path(self):
        i = issuebot.IssuebotModule()
        for path in (
            'issuebot/tests/info.zwanenburg.caffeinetile_4.apk',
            'issuebot/tests/minimal_targetsdk_30_unsigned.apk',
            'issuebot/tests/org.bitbucket.tickytacky.mirrormirror_4.apk',
        ):
            self.assertEqual([], i.get_urls_from_apk(path))

        for apk, urls in {
            'issuebot/tests/com.github.wakhub.tinyclock_5.apk': [
                'https://play.google.com/store/apps/details?id=com.github.wakhub.tinyclock'
            ],
            'issuebot/tests/org.droidtr.deletegapps_15.apk': [
                'https://gitlab.com/sulincix/Guvenli_Internet/raw/master/hosts',
                'http://t.me/antigapps',
            ],
            'issuebot/tests/net.tevp.postcode_3.apk': [
                'http://www.uk-postcodes.com/latlng/%.8f,%.8f.json',
                'http://api.postcodes.io/postcodes?lon=%.8f&lat=%.8f&limit=1',
                'http://ws.geonames.org/findNearbyPostalCodesJSON?lat=%.8f&lng=%.8f&maxRows=1',
            ],
            'issuebot/tests/com.pikselbit.wrongpinshutdown_1.apk': [
                'https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=admin%40snslocation%2ecom&lc=US&item_name=Wrong%20Pin%20Shutdown&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted'
            ],
            'issuebot/tests/orbitlivewallpaperfree.puzzleduck.com_1.apk': [
                'https://github.com/PuZZleDucK/Orbital-Live-Wallpaper',
                'http://db.tt/41Y5NAS',
            ],
        }.items():
            self.assertEqual(urls, i.get_urls_from_apk(apk))

    def test_read_properties(self):
        with TemporaryDirectory() as tmpdir:
            os.chdir(tmpdir)
            path = Path('gradle-wrapper.properties')
            text = textwrap.dedent(
                """
                #Wed Apr 10 15:27:10 PDT 2013
                distributionUrl=https\\://services.gradle.org/distributions/gradle-7.5.1-all.zip
                distributionSha256Sum=0123456789abcdef
                """
            )
            path.write_text(text)
            propdata, properties = issuebot.read_properties(path)
            self.assertEqual(text, propdata)
            self.assertEqual(
                properties,
                {
                    'distributionSha256Sum': '0123456789abcdef',
                    'distributionUrl': 'https://services.gradle.org/distributions/gradle-7.5.1-all.zip',
                },
            )

    def test_duplicate_keys_in_properties(self):
        """https://gitlab.com/fdroid/fdroidserver/-/issues/1131"""
        with TemporaryDirectory() as tmpdir:
            os.chdir(tmpdir)
            path = Path('gradle-wrapper.properties')
            path.write_text(
                textwrap.dedent(
                    """
                    distributionSha256Sum=0000
                    distributionSha256Sum =1111
                    distributionSha256Sum = 2222
                    k = v
                    """
                )
            )
            propdata, properties = issuebot.read_properties(path)
            self.assertEqual(properties, {'distributionSha256Sum': '2222', 'k': 'v'})

    def test_properties_get_keys(self):
        with TemporaryDirectory() as tmpdir:
            os.chdir(tmpdir)
            path = Path('gradle-wrapper.properties')
            path.write_text(
                textwrap.dedent(
                    """
                    #Wed Apr 10 15:27:10 PDT 2013
                    distributionSha256Sum=0000
                    distributionSha256Sum =1111
                    distributionSha256Sum = 2222
                    equals\\==1
                    arg\\= =1
                    distributionUrl\\=https\\://services.gradle.org/distributions/gradle-2.1-bin.zip
                    distributionUrl=http\\://services.gradle.org/distributions/bad-bin.zip
                    """
                )
            )
            keys = issuebot.read_properties_keys(path)
            self.assertEqual(
                keys,
                [
                    'distributionSha256Sum',
                    'distributionSha256Sum',
                    'distributionSha256Sum',
                    'equals\\=',
                    'arg\\=',
                    'distributionUrl\\=https\\://services.gradle.org/distributions/gradle-2.1-bin.zip',
                    'distributionUrl',
                ],
            )
            duplicates = [i for i in set(keys) if keys.count(i) > 1]
            self.assertEqual(duplicates, ['distributionSha256Sum'])

    def test_etip_network_search(self):
        i = issuebot.IssuebotModule()
        for host in ['example.com', 'foo.bar.com', 'www.kernel.org', 'noads.org']:
            self.assertEqual([], i.etip_network_search(host))
        self.assertEqual(
            [
                (
                    'https://etip.exodus-privacy.eu.org/trackers/25907b48-f816-45ce-aa66-4b7c5e1bf666/',
                    'adswizz\\.com',
                )
            ],
            i.etip_network_search('www.adswizz.com'),
        )

    mock_etip_api_key_ids = {
        '7abe4da3-47cd-4af2-b0c1-480253550a51': [
            re.compile(r'GPA\.[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4,5}(?:\.\.[0-9]+)?'),
        ],
        '0be1df31-46ed-4b0f-b788-48149ec36634': [
            re.compile(r'com\.huawei\.hms\.client\.appid'),
            re.compile(r'com\.huawei\.hms\.client\.service\.name:hianalytics'),
            re.compile(r'com\.huawei\.hms\.client\.service\.name:location'),
            re.compile(r'com\.huawei\.hms\.min_api_level:hianalytics:hianalytics'),
            re.compile(r'com\.huawei\.hms\.min_api_level:location:location'),
        ],
    }

    @mock.patch('issuebot.IssuebotModule._get_etip')
    def test_etip_api_key_id_search(self, get_etip):
        i = issuebot.IssuebotModule()
        assert get_etip.called
        i.etip_api_key_ids = self.mock_etip_api_key_ids
        self.assertEqual([], i.etip_api_key_id_search('this is just a test'))
        self.assertEqual(
            [
                (
                    i.get_etip_tracker_url('0be1df31-46ed-4b0f-b788-48149ec36634'),
                    'com\\.huawei\\.hms\\.min_api_level:location:location',
                ),
            ],
            i.etip_api_key_id_search(
                '<meta-data android:name="com.huawei.hms.min_api_level:location:location" android:value="foo"/>'
            ),
        )
        self.assertEqual(
            [
                (
                    i.get_etip_tracker_url('7abe4da3-47cd-4af2-b0c1-480253550a51'),
                    'GPA\\.[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4,5}(?:\\.\\.[0-9]+)?',
                )
            ],
            i.etip_api_key_id_search(
                '<string name="google_play_billing">GPA.1234-5678-9012-34567..1</string>'
            ),
        )


if __name__ == "__main__":
    unittest.main()

#!/usr/bin/env python3

import gitlab
import glob
import json
import io
import issuebot
import os
import re
import shutil
import subprocess
import sys
import yaml
import zipfile

from colorama import Fore, Style


APPLICATIONIDS_FILE = os.path.join(issuebot.ISSUEBOT_API_DIR, 'applicationIds.json')
SOURCEURLS_FILE = os.path.join(issuebot.ISSUEBOT_API_DIR, 'sourceUrls.json')
APPID_REGEX = re.compile(r'metadata/(.+)\.yml')


def download_report_history(project):
    """use gitlab API to fetch entire history of reports

    This gets the artifacts from all the jobs listed for this project,
    unpack public/issuebot from them this will provide a complete,
    available history.

    """

    class Extractor(object):
        def __init__(self):
            self._fd = io.BytesIO()

        def __call__(self, chunk):
            self._fd.write(chunk)

        def extract(self):
            with zipfile.ZipFile(self._fd) as zipfp:
                for zipinfo in zipfp.infolist():
                    if (
                        zipinfo.filename.startswith(issuebot.ISSUEBOT_API_DIR)
                        and zipinfo.file_size > 0
                    ):
                        outdir = os.path.dirname(zipinfo.filename)
                        os.makedirs(outdir, exist_ok=True)
                        zipfp.extract(zipinfo)

    print('----------------------------\nGetting Job History for API\n')
    for pipeline in project.pipelines.list():
        print(pipeline.web_url)
        for pipeline_job in pipeline.jobs.list():
            job = project.jobs.get(pipeline_job.id, lazy=True)
            try:
                target = Extractor()
                job.artifacts(streamed=True, action=target)
                target.extract()
            except gitlab.exceptions.GitlabGetError as e:
                print(
                    Fore.YELLOW
                    + (
                        'WARNING: artifacts for pipeline %d job %d (%s) are %s'
                        % (pipeline.id, pipeline_job.id, pipeline_job.status, str(e))
                    )
                    + Style.RESET_ALL
                )
            if os.path.exists(APPLICATIONIDS_FILE):
                with open(APPLICATIONIDS_FILE) as fp:
                    data = json.load(fp)
                os.remove(APPLICATIONIDS_FILE)
                for appid, job_list in data.items():
                    if appid not in issuebot.applicationIds_data:
                        issuebot.applicationIds_data[appid] = []
                    issuebot.applicationIds_data[appid] += job_list
            if os.path.exists(SOURCEURLS_FILE):
                with open(SOURCEURLS_FILE) as fp:
                    data = json.load(fp)
                os.remove(SOURCEURLS_FILE)
                for sourceUrl, job_list in data.items():
                    if sourceUrl not in issuebot.sourceUrls_data:
                        issuebot.sourceUrls_data[sourceUrl] = []
                    issuebot.sourceUrls_data[sourceUrl] += job_list

    for job_dir in glob.glob(
        os.path.join(issuebot.ISSUEBOT_API_DIR, '[0-9][0-9][0-9][0-9]*')
    ):
        if not os.path.isdir(job_dir):
            continue
        job_id = os.path.basename(job_dir)
        for issue_dir in glob.glob(os.path.join(job_dir, '[0-9]*')):
            if not os.path.isdir(issue_dir):
                continue
            issue_id = os.path.basename(issue_dir)
            appid = None
            sourceUrl = None
            module_reports = []
            for module_report in glob.glob(os.path.join(issue_dir, '*.json')):
                module_reports.append(
                    os.path.relpath(module_report, issuebot.ISSUEBOT_API_DIR)
                )
                if appid is None or sourceUrl is None:
                    with open(module_report) as fp:
                        data = json.load(fp)
                    appid = data.get('applicationId')
                    sourceUrl = data.get('sourceUrl')
            if appid:
                issuebot.append_to_applicationIds_data(
                    job_id, issue_id, appid, module_reports
                )
            if sourceUrl:
                issuebot.append_to_sourceUrls_data(
                    job_id, issue_id, sourceUrl, module_reports
                )

    for appid, job_list in issuebot.applicationIds_data.items():
        jobs = set()
        new_list = []
        for job in job_list:
            if job['jobId'] not in jobs:
                new_list.append(job)
            jobs.add(job['jobId'])
            issuebot.applicationIds_data[appid] = sorted(
                new_list, reverse=True, key=lambda item: item['jobId']
            )

    for sourceUrl, job_list in issuebot.sourceUrls_data.items():
        jobs = set()
        new_list = []
        for job in job_list:
            if job['jobId'] not in jobs:
                new_list.append(job)
            jobs.add(job['jobId'])
            issuebot.sourceUrls_data[sourceUrl] = sorted(
                new_list, reverse=True, key=lambda item: item['jobId']
            )


def update_entry_index(name, data):
    """Merge the current data with the published archive

    This waits forever until it can fetch the archive to avoid losing
    data.  If we can't get the archive, then the whole job run should
    be cancelled.  This makes issuebot quit if there are
    network/server issues.  Issuebot will run again on a schedule, so
    its better to abort this job run rather than lose the data archive
    that's stored on GitLab Pages.

    If the URL gives back 404 Not Found, then that means this is the
    first run, and there is no archive.  It seems GitLab Pages might
    also return 503 when it is uninitialized.

    """
    group, project = os.getenv('CI_PROJECT_PATH').split('/')
    url = 'https://{group}.gitlab.io/{project}/issuebot/{name}.json'.format(
        group=group, project=project, name=name
    )
    archive = None
    while True:
        r = issuebot.requests_get(url)
        if r.status_code in (404, 503):
            archive = dict()
            break
        if r.status_code == 200:
            archive = r.json()
            break
        print('Trying again:', r.status_code, url)
    for appid, entry in data.items():
        if appid in archive:
            archive[appid].insert(0, entry[0])
        else:
            archive[appid] = entry
    output = dict()
    for appid, entries in archive.items():
        jobIds = set()
        for entry in entries:
            if entry['jobId'] not in jobIds:
                if appid not in output:
                    output[appid] = []
                output[appid].append(entry)
            jobIds.add(entry['jobId'])
    os.makedirs(issuebot.ISSUEBOT_API_DIR, exist_ok=True)
    print('WRITING', os.path.join(issuebot.ISSUEBOT_API_DIR, name + '.json'))
    with open(os.path.join(issuebot.ISSUEBOT_API_DIR, name + '.json'), 'w') as fp:
        json.dump(output, fp, indent=2, sort_keys=True)


def get_valid_merge_request(project):
    """Get a merge_request instance if issuebot was triggered by a merge request

    If ci_pipeline_id is an int, we're running in a merge request that
    triggered us.  Then fetch the merge request info, see if it
    changes a metadata file, and if so, then set up the local repo for
    working with.  This GitLab CI Job must have been started by a
    trigger script for it to receive the FROM_CI_* variables.

    """
    try:
        if os.getenv('CI_PROJECT_NAME') == 'rfp':
            print('Skipping merge request logic in project called "rfp"')
            return

        ci_merge_request_iid = int(os.getenv('FROM_CI_MERGE_REQUEST_IID'))
    except (TypeError, ValueError) as e:
        print(
            Fore.RED
            + 'ERROR: FROM_CI_MERGE_REQUEST_IID env var '
            + str(e)
            + Style.RESET_ALL
        )
        return

    merge_request = None
    try:
        merge_request = project.mergerequests.get(ci_merge_request_iid)
        if merge_request is not None:
            return merge_request
    except gitlab.exceptions.GitlabGetError as e:
        print(Fore.YELLOW + 'WARNING: ' + str(e) + Style.RESET_ALL)
    try:
        ci_pipeline_id = int(os.getenv('FROM_CI_PIPELINE_ID'))
    except (TypeError, ValueError) as e:
        print(
            Fore.RED + 'ERROR: FROM_CI_PIPELINE_ID env var ' + str(e) + Style.RESET_ALL
        )
        return
    for mr in project.mergerequests.list(
        lazy=True, per_page=250, state='opened', order_by='updated_at'
    ):
        for pipeline in mr.pipelines():
            if ci_pipeline_id == pipeline['id']:
                merge_request = mr
                break
        if merge_request:
            break
    return merge_request


def get_builds_in_merge_request(gl, merge_request):
    """Compare a merge request commits to master to find the changes

    This works just like tools/find-changed-builds.py in fdroiddata's
    `fdroid build` job, since that is how gitlab-ci generally
    operates.  Here, issuebot starts on the older commit, so the `git
    checkout` command needs to be run before this function.  In
    fdroiddata's `fdroid build` job, the script runs starting at the
    newer commit.

    """
    # find HEAD commit of the merge request's fork
    commit_id = merge_request.sha
    if not commit_id:
        commit_id = os.getenv('FROM_CI_COMMIT_SHA')
    if not commit_id:
        print(
            Fore.RED
            + 'ERROR: invalid FROM_CI_COMMIT_SHA: "{}"'.format(str(commit_id))
            + Style.RESET_ALL
        )
        return [], []
    os.chdir(os.getenv('CI_PROJECT_DIR'))
    source_project = gl.projects.get(merge_request.source_project_id)
    source_project_url = source_project.http_url_to_repo
    if not source_project_url:
        source_project_url = os.getenv('FROM_CI_PROJECT_URL')
    if not source_project_url or not commit_id:
        print(
            Fore.YELLOW
            + 'WARNING: cannot process merge request builds without source URL and commit ID'
            + Style.RESET_ALL
        )
        return [], []
    issuebot.run_cli_tool(['git', 'fetch', source_project_url, commit_id])
    issuebot.run_cli_tool(['git', 'reset', '--hard', commit_id])
    upstream_commit_id = os.getenv('CI_COMMIT_SHA')
    output = subprocess.check_output(
        [
            'git',
            'diff',
            '--name-only',
            '--diff-filter=d',
            '%s...%s' % (upstream_commit_id, commit_id),
        ]
    )
    changed_appids = set()
    for i in output.decode().split():
        m = APPID_REGEX.match(i)
        if m:
            changed_appids.add(m.group(1))
    builds = dict()
    for appid in changed_appids:
        builds[appid] = []
        metadata_file = issuebot.get_metadata_file(appid)
        diff = subprocess.check_output(
            [
                'git',
                'diff',
                '--no-color',
                '--diff-filter=d',
                '%s...%s' % (upstream_commit_id, commit_id),
                '--',
                metadata_file,
            ]
        )

        with open(metadata_file) as fp:
            current = yaml.safe_load(fp)
        cmd = 'git apply --reverse'
        p = subprocess.run(cmd.split(' '), input=diff, stdout=subprocess.DEVNULL)
        if p.returncode:
            print(
                Fore.RED + ('ERROR: %s: %d' % (cmd, p.returncode)) + Style.RESET_ALL,
                file=sys.stderr,
            )
            continue
        to_build = []
        if os.path.exists(metadata_file):
            with open(metadata_file) as fp:
                previous = yaml.safe_load(fp)
            cmd = 'git apply'
            p = subprocess.run(cmd.split(' '), input=diff, stdout=subprocess.DEVNULL)
            if p.returncode:
                print(
                    Fore.RED
                    + ('ERROR: %s: %d' % (cmd, p.returncode))
                    + Style.RESET_ALL,
                    file=sys.stderr,
                )
                continue

            previous_builds = dict()
            for build in previous['Builds']:
                previous_builds[build['versionCode']] = build

            for build in current['Builds']:
                vc = build['versionCode']
                if vc not in previous_builds:
                    to_build.append(vc)
                    continue
                if build != previous_builds[vc]:
                    to_build.append(vc)
        else:
            # this is a brand new metadata file
            cmd = 'git checkout -- ' + metadata_file
            p = subprocess.run(cmd.split(' '), stdout=subprocess.DEVNULL)
            if p.returncode:
                print(
                    Fore.RED
                    + ('ERROR: %s: %d' % (cmd, p.returncode))
                    + Style.RESET_ALL,
                    file=sys.stderr,
                )
                sys.exit(p.returncode)
            with open(metadata_file) as fp:
                data = yaml.safe_load(fp)
            for build in data['Builds']:
                to_build.append(build['versionCode'])

        for vc in to_build:
            builds[appid].append('%s:%d' % (appid, vc))

    return changed_appids, builds


def main():
    private_token = os.getenv('PERSONAL_ACCESS_TOKEN')
    if not private_token:
        print(
            Fore.RED
            + 'ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!'
            + Style.RESET_ALL
        )
        sys.exit(1)
    gl = gitlab.Gitlab('https://gitlab.com', api_version=4, private_token=private_token)
    gl.auth()  # ensure gl.user is present
    project = gl.projects.get(os.getenv('CI_PROJECT_PATH'), lazy=True)

    download_report_history(project)

    merge_request = get_valid_merge_request(project)
    appid = None
    builds = []
    to_process = None

    if merge_request:
        appids, builds = get_builds_in_merge_request(gl, merge_request)
        print('This merge request changed these apps:')
        print('\n'.join(appids))
        print('and these specific build entries:')
        for args in builds.values():
            for arg in args:
                print(arg)
        # remove extra metadata/ files from processing and artifacts
        for f in glob.glob('metadata/*.*'):
            if os.path.isdir(f) and os.path.basename(f) not in appids:
                shutil.rmtree(f, ignore_errors=True)
            elif os.path.isfile(f) and os.path.basename(f)[:-4] not in appids:
                os.remove(f)
        if len(appids) > 0:
            to_process = merge_request
    else:
        to_process = project.issues.list(
            state='opened', order_by='updated_at', per_page=250
        )

    if to_process:
        issuebot.create_dummy_fdroid_repo()
        issuebot.setup_for_modules()

    if isinstance(to_process, gitlab.v4.objects.ProjectMergeRequest):
        for appid in appids:
            issuebot.process_issue_or_merge_request(
                project, to_process, appid, builds=builds[appid], user_id=gl.user.id
            )
    elif to_process is not None:
        for issue in to_process:
            if issuebot.PROCESSED > 5:
                print('Processed %d items, quitting.' % issuebot.PROCESSED)
                break
            issuebot.process_issue(project, issue, gl.user.id)

    update_entry_index('applicationIds', issuebot.applicationIds_data)
    update_entry_index('sourceUrls', issuebot.sourceUrls_data)


if __name__ == "__main__":
    main()

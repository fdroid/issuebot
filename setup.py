#!/usr/bin/env python3

from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()


setup(
    name='issuebot',
    version='0.1',
    description='A bot for responding to F-Droid issues and merge requests.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='The F-Droid Project',
    author_email='team@f-droid.org',
    url='https://f-droid.org',
    license='AGPL-3.0',
    packages=['issuebot'],
    scripts=['issuebot.py'],
    python_requires='>=3.9',
    install_requires=[
        'defusedxml',
        'fdroidserver >= 2.0a4',
        'javaproperties',
        'pygithub',
        'python-gitlab',
        'PyYAML',
        'requests >= 2.5.2, != 2.11.0, != 2.12.2, != 2.18.0',
        'requests_cache',
    ],
    test_suite='nose.collector',
    tests_require=['nose'],
    extras_require={'modules': ['pycares']},
    classifiers=[
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Unix',
        'Topic :: Utilities',
    ],
)
